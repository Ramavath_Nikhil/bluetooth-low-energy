package com.dtq.ble;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.UUID;

import static android.bluetooth.BluetoothProfile.STATE_CONNECTED;
import static android.bluetooth.BluetoothProfile.STATE_DISCONNECTED;

/**
 * Created by nikhilramavath on 22/03/18.
 */
@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
public class BLEIntentService extends Service {


    private BluetoothAdapter bluetoothAdapter;
    private BluetoothGatt gatt;

    private String READ_CHARACTERSTICS = "0783b03e-8535-b5a0-7140-a304d2495cb8";
    private String WRITE_CHARACTERSTICS = "0783b03e-8535-b5a0-7140-a304d2495cba";
    private String SERVICE_UUID = "0783b03e-8535-b5a0-7140-a304d2495cb7";
    private String DESCRIPTIOR_UUID = "00002902-0000-1000-8000-00805f9b34fb";

    public class LocalBinder extends Binder {
        public BLEIntentService getService() {
            return BLEIntentService.this;
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

//        if (intent != null && intent.getAction() != null && intent.getAction().equals("connect"))
        connectToBle();
        return super.onStartCommand(intent, flags, startId);

    }

    public void connectToBle() {

        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        if (bluetoothManager != null) {

            bluetoothAdapter = bluetoothManager.getAdapter();
            if (bluetoothAdapter != null) {
                Log.e("scan", "Started");
                bluetoothAdapter.startLeScan(scanCallback);

            }


        }


    }


    final BluetoothAdapter.LeScanCallback scanCallback =
            new BluetoothAdapter.LeScanCallback() {
                @Override
                public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
                    Log.d("DEVICE NAMES", device.getName() + "  ");
                    Log.d("DEVICE Address", device.getAddress() + "  ");
                    if (device.getName() != null && device.getName().equalsIgnoreCase("OmniLock")) {


                        gatt = device.connectGatt(getBaseContext(), true, gattCallback);


                        if (bluetoothAdapter != null) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                bluetoothAdapter.stopLeScan(scanCallback);
                            }

                        }


                    }


                }


            };


    public UUID convertFromInteger(int i) {
        final long MSB = 0x0000000000001000L;
        final long LSB = 0x800000805f9b34fbL;
        long value = i & 0xFFFFFFFF;
        return new UUID(MSB | (value << 32), LSB);
    }


    BluetoothGattCallback gattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);


            if (newState == STATE_CONNECTED) {
                Log.e("gatt connected", "ytes");

                gatt.discoverServices();
            } else if (newState == STATE_DISCONNECTED) {
                Log.e("gatt condisnected", "ytes");
            } else {
                Log.e("new status", newState + "");
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {

            Log.e("service discoverd", "yes");


            BluetoothGattService bluetoothGattService = gatt.getService(UUID.fromString(SERVICE_UUID));

            if (bluetoothGattService != null) {

                for (BluetoothGattCharacteristic bluetoothGattCharacteristic : gatt.getService(UUID.fromString(SERVICE_UUID)).getCharacteristics()) {

                    Log.e("charactersri discoverd", "" + bluetoothGattCharacteristic.getUuid());
                }
                BluetoothGattCharacteristic characteristic = bluetoothGattService.getCharacteristic(UUID.fromString(READ_CHARACTERSTICS));

                if (gatt.setCharacteristicNotification(characteristic, true))
                    Log.e("connection set", "sne");
                else
                    Log.e("connectinon not", "Sent");


                for (BluetoothGattDescriptor bluetoothGattDescriptor : characteristic.getDescriptors()) {
                    Log.e("descr", String.valueOf(bluetoothGattDescriptor.getUuid()));
                }


                BluetoothGattDescriptor descriptor = characteristic.getDescriptor(UUID.fromString(DESCRIPTIOR_UUID));
                descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                if (gatt.writeDescriptor(descriptor))
                    Log.e("descriptor", "success");
                else
                    Log.e("descriptro", "fail");


//                BluetoothGattDescriptor writeDescriptor = writeCharacterstic.getDescriptor(UUID.fromString(DESCRIPTIOR_UUID));
//                writeDescriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
//                if (gatt.writeDescriptor(writeDescriptor))
//                    Log.e("descriptor", "success");
//                else
//                    Log.e("descriptro", "fail");


//
//                if (charac == null) {
//                    Log.d("s", "char not found!");
//
//                } else {
//                    Log.e("char found", "");
//
//
//                }


            } else {
                Log.e("BLe service", "null");
            }


        }


        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor,
                                      int status) {
//            BluetoothGattCharacteristic characteristic =
//                    gatt.getService(UUID.fromString(SERVICE_UUID))
//                            .getCharacteristic(HEART_RATE_CONTROL_POINT_CHAR_UUID);
//            characteristic.setValue(new byte[]{1, 1});
//            gatt.writeCharacteristic(characteristic);

            Log.e("onDescriptorWrite", "a");

            BluetoothGattService bluetoothGattService = gatt.getService(UUID.fromString(SERVICE_UUID));

            BluetoothGattCharacteristic writeCharacterstic = bluetoothGattService.getCharacteristic(UUID.fromString(WRITE_CHARACTERSTICS));
            Log.e("a", writeCharacterstic.getDescriptors().size() + " " + writeCharacterstic.getUuid());

            for (BluetoothGattDescriptor bluetoothGattDescriptor : writeCharacterstic.getDescriptors()) {
                Log.e("descr", String.valueOf(bluetoothGattDescriptor.getUuid()));
            }
//            char[] key = {'y', 'O', 'T', 'm', 'K', '5', '0', 'z'};
//            char[] key = { '5','b','p','j','8','p','T','G'};
            String key = "5bpj8pTG";
            try {
                byte[] data = key.getBytes("UTF-8");


                int NUM = 0x88;
                int NUM_1 = NUM + 0x32;
                byte[] bytes = new byte[13];
                Log.e("asd", String.valueOf(0xFE) + " " + Integer.valueOf("254").byteValue());
                bytes[0] = (byte) 0xFE;
                bytes[1] = (byte) (NUM_1);
                byte KEY = (byte) (0x00 ^ NUM);
                bytes[2] = KEY;
                byte CMD = (byte) (0x11 ^ NUM);
                bytes[3] = CMD;
                byte LEN = (byte) (0x08 ^ NUM);
                bytes[4] = LEN;


                for (int i = 5, j = 0; i < bytes.length; i++, j++) {
                    Log.d("data inside loop", data[j] + " ");
                    bytes[i] = (byte) (data[j] ^ NUM);
                    Log.d("data inside loop", bytes[i] + " ");
                }
                int crc = crc16(bytes);
                Log.d("CRC", crc + "");
                byte[] input = new byte[bytes.length + 2];

                for (int i = 0; i < bytes.length; i++) {
                    input[i] = bytes[i];
                }

                input[input.length - 2] = (byte) (crc >> 8); // crc16
                input[input.length - 1] = (byte) crc;


                Log.e("string", Arrays.toString(input));
                writeCharacterstic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
                writeCharacterstic.setValue(input);
                if (gatt.writeCharacteristic(writeCharacterstic)) {
                    Log.e("Send", "s");
                } else
                    Log.e("not sent", "s");


            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }


        }

        @Override
        public void onReliableWriteCompleted(BluetoothGatt gatt, int status) {
            super.onReliableWriteCompleted(gatt, status);
            Log.e("onReliableWrite", "onReliableWriteCompleted");
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicWrite(gatt, characteristic, status);

            Log.e("onCharacteristic", "onCharacteristicWrite");

        }

        @Override
        public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorRead(gatt, descriptor, status);
            Log.e("onDescriptorread", "onDescriptorRead");

        }

        @Override
        public void onMtuChanged(BluetoothGatt gatt, int mtu, int status) {
            super.onMtuChanged(gatt, mtu, status);
            Log.e("onMtuChanged", "onMtuChanged");
        }

        @Override
        public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
            super.onReadRemoteRssi(gatt, rssi, status);
            Log.e("onReadRemoteRssi", "");
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicRead(gatt, characteristic, status);
            Log.e("read", "read " + Arrays.toString(characteristic.getValue()));
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic
                characteristic) {
            Log.e("onCharacteristicChanged", "yes " + characteristic.getUuid() + " " + Arrays.toString(characteristic.getValue()));


            byte[] data = characteristic.getValue();

            if (data.length >= 8) {
                int NUM = data[1] - 0x32 & 0xFF;
                Log.e("NUM", NUM + "");
                int Key = data[2] ^ NUM;
                Log.e("secret key", String.valueOf(Key));
                int CMD = data[3] ^ NUM;
                Log.e("CMD", CMD + "");
                int LEN = data[4] ^ NUM;
                Log.e("LEN", LEN + "");

                // apply decode to all data (but not header and crc data
                for (int x = 5; x < data.length - 2; x++) {
                    data[x] = (byte) (data[x] ^ NUM);
                    Log.e("data " + x, data[x] + "");
                }


                int _NUM = 0x88;
                int _NUM_1 = _NUM + 0x32;
                byte[] _bytes = new byte[13];
                Log.e("asd", String.valueOf(0xFE) + " " + Integer.valueOf("254").byteValue());
                _bytes[0] = (byte) 0xFE;
                _bytes[1] = (byte) (_NUM_1);
//                byte KEY = (byte) (0x00 ^ NUM);
                _bytes[2] = (byte) ((byte) Key^_NUM);
                byte _CMD = (byte) (0x21 ^ _NUM);
                _bytes[3] = _CMD;
                byte _LEN = (byte) (0x08 ^ _NUM);
                _bytes[4] = _LEN;

                _bytes[5] = (byte) (0 ^ _NUM);
                _bytes[6] = (byte) (0 ^ _NUM);
                _bytes[7] = (byte) (0 ^ _NUM);
                _bytes[8] = (byte) (52 ^ _NUM);

                String timeStamp = Long.toHexString(Math.round(Calendar.getInstance().getTimeInMillis() / 1000));


                _bytes[9] = (byte) (Integer.parseInt(timeStamp.substring(0, 2),16)^_NUM);

//                Log.e("timestamp", timeStamp.substring(0, 2) + " " + Integer.parseInt(timeStamp.substring(0, 2) + " " + ((byte) Integer.parseInt(timeStamp.substring(0, 2),16))));
                _bytes[10] = (byte) (Integer.parseInt(timeStamp.substring(2, 4),16)^_NUM);
//                Log.e("timestamp", timeStamp.substring(2, 4) + " " + Integer.parseInt(timeStamp.substring(2, 4) + " " + ((byte) Integer.parseInt(timeStamp.substring(2, 4),16))));

                _bytes[11] = (byte) (Integer.parseInt(timeStamp.substring(4, 6),16)^_NUM);
//                Log.e("timestamp", timeStamp.substring(4, 6) + " " + Integer.parseInt(timeStamp.substring(4, 6) + " " + ((byte) Integer.parseInt(timeStamp.substring(4, 6),16))));

                _bytes[12] = (byte) (Integer.parseInt(timeStamp.substring(6, 8),16)^_NUM);
//                Log.e("timestamp", timeStamp.substring(6, 8) + " " + Integer.parseInt(timeStamp.substring(6, 8) + " " + ((byte) Integer.parseInt(timeStamp.substring(6, 8),16))));

                int crc = crc16(_bytes);

                byte[] input = new byte[_bytes.length + 2];

                for (int i = 0; i < _bytes.length; i++) {
                    input[i] = _bytes[i];
                }

                input[input.length - 2] = (byte) (crc >> 8); // crc16
                input[input.length - 1] = (byte) crc;


                Log.e("string", Arrays.toString(input));
                BluetoothGattService bluetoothGattService = gatt.getService(UUID.fromString(SERVICE_UUID));
                BluetoothGattCharacteristic writeCharacterstic = bluetoothGattService.getCharacteristic(UUID.fromString(WRITE_CHARACTERSTICS));

                writeCharacterstic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
                writeCharacterstic.setValue(input);
                if (gatt.writeCharacteristic(writeCharacterstic)) {
                    Log.e("Send", "s");
                } else
                    Log.e("not sent", "s");


            }
        }

    };

    static int crc16(final byte[] buffer) {

//        var crc = 0xFFFF;
//        var odd;
//
//        for (var i = 0; i < buffer.length; i++) {
//            crc = crc ^ buffer[i];
//
//            for (var j = 0; j < 8; j++) {
//                odd = crc & 0x0001;
//                crc = crc >> 1;
//                if (odd) {
//                    crc = crc ^ 0xA001;
//                }
//            }
//        }
//
//        return crc;
//        int crc = 0xFFFF;
////        int odd;
//        for (int j = 0; j < buffer.length; j++) {
////            crc = crc ^ buffer[j];
////            for (int i = 0; i < 8; j++) {
////                odd = crc & 0x0001;
////                crc = crc >> 1;
////                if (odd == 0) {
////                    crc = crc ^ 0xA001;
////                }
////            }
//            crc = ((crc >>> 8) | (crc << 8)) & 0xffff;
//            crc ^= (buffer[j] & 0xff);//byte to int, trunc sign
//            crc ^= ((crc & 0xff) >> 4);
//            crc ^= (crc << 12) & 0xffff;
//            crc ^= ((crc & 0xFF) << 5) & 0xffff;
//        }
//        crc &= 0xffff;

        return CRCUtil.calcCRC(buffer);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("service destroyed", "yes");

        if (gatt != null)
            gatt.disconnect();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onStart(@Nullable Intent intent, int startId) {
        super.onStart(intent, startId);
        Log.e("sterted", "er");
    }
}

