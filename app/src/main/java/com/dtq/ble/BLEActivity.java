package com.dtq.ble;

import android.Manifest;
import android.app.ActivityManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

public class BLEActivity extends AppCompatActivity {

    private static final int REQUEST_ENABLE_BT = 1;
    private static final int REQUEST_PERMISSION_COURSE_LOCATION = 2;
    private BLEIntentService mBluetoothLeService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ble);

    }


    private void connectToBLEService() {

        BluetoothAdapter bluetoothAdapter;
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        if (bluetoothManager != null) {
            bluetoothAdapter = bluetoothManager.getAdapter();
            if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
                Intent enableBtIntent =
                        new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }


            if (ContextCompat.checkSelfPermission(getBaseContext(), Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(BLEActivity.this,
                        Manifest.permission.READ_CONTACTS)) {

                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                } else {


                    ActivityCompat.requestPermissions(BLEActivity.this,
                            new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                            REQUEST_PERMISSION_COURSE_LOCATION);


                }


            } else {
                Log.e("Starting", "asd");
                Intent intent = new Intent(BLEActivity.this, BLEIntentService.class);
                intent.setAction("connect");
                startService(new Intent(BLEActivity.this, BLEIntentService.class));
            }


        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (!isMyServiceRunning(BLEIntentService.class)) {

            Log.e("Service", "not runnig");
            connectToBLEService();
        } else {
            Log.e("service", " running");
        }

    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        if (manager != null) {
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.getName().equals(service.service.getClassName())) {
                    return true;
                }
            }
        }
        return false;
    }


    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {

            Log.e("bind", "");
            mBluetoothLeService = ((BLEIntentService.LocalBinder) service).getService();

            if (mBluetoothLeService != null) {

                Log.e("not null", "");


            } else {
                Log.e("null", "");

            }


            // Automatically connects to the device upon successful start-up initialization.
            //  mBluetoothLeService.connect(mDeviceAddress);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;

        }


    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Intent intent = new Intent(BLEActivity.this, BLEIntentService.class);
        intent.setAction("connect");
        startService(new Intent(BLEActivity.this, BLEIntentService.class));
    }

    public void stopBLE(View view) {

        Intent intent = new Intent(BLEActivity.this, BLEIntentService.class);
        stopService(intent);
    }
}
